var five = require("johnny-five");


five.Board().on("ready", function() {
  var piezo = new five.Piezo(8);

  var temperature = new five.Temperature({
    controller: "TMP36",
    pin: "A4"
  });

  temperature.on("change", function(err, data) {
    if(data.celsius >= 50){
      piezo.frequency(700, 5000);
    }else{
      piezo.frequency(0, 0);
    }
  });
});
